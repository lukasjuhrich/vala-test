using Gtk;

int main(string[] args){
    init(ref args);

	var header = new HeaderBar();

	var window = new Window();
	window.set_titlebar(header);
	window.destroy.connect(main_quit);

	window.set_default_size(350, 70);
	window.border_width = 10;

	var button_title = "Hello!";
	window.add(new Button.with_label(button_title));
	window.show_all();

	Gtk.main();
    return 0;
}